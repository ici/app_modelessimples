# App_ModelesSimples

Pour aller sur la page HTML :
1. Cliquez sur le lien Binder : 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fici%2Fapp_modelessimples/HEAD)
2. Attendre quelques dizaines de secondes (le premiere lancement peut etre long => 1 minute )
3. Vous devriez voir une arborescence, cliquez sur HTML_modeles_simples.ipynb
4. Un jupyter notebook s'ouvre, cliquez alors sur l'onglet Voila présent dans la barre d'outil
5. La page s'affiche alors. Il faut attendre quelques secondes pour que les barres interactives s'affichent
6. Enjoy!
